/*
Boiler
Copyright (C) 2015  Ansas Bogdan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MatchListModel_h__
#define MatchListModel_h__
#include <QAbstractTableModel>
#include <vector>
#include "CSGOMatchList.h"

/**
 * manages the match information
 */
class MatchListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum class Columns
    {
        Date,
        Result,
        Kills,
        Assits,
        Deaths,
        MVPs,
        Score,
        KD,
        Link,
        Count
    };

    MatchListModel(QObject* parent);
    ~MatchListModel();

    virtual int rowCount(const QModelIndex&  parent = QModelIndex()) const final;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const final;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const final;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const final;

    void RefreshData();
    CSGOMatchList& MatchList() { return m_matchList; }

private slots:
    void onAddMatches(int num);

private:
    CSGOMatchList m_matchList;
};

#endif // MatchListModel_h__
