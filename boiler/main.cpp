/*
Boiler
Copyright (C) 2015  Ansas Bogdan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QApplication>
#include <thread>
#include <future>
#include <QMessageBox>
#include <QFile>
#include <QSharedMemory>
#include "UI.h"
#include "CSGOMatchList.h"
#include "BoilerException.h"
#include <iostream>
#include <io.h>
#include <fcntl.h>

static bool s_refreshOnly = false;

void Error(const char* title, const char* text)
{
    if (!s_refreshOnly)
        QMessageBox::critical(nullptr, title, text);
    else
        printf("%s: %s\n", title, text);
}


int main(int argc, char** argv)
{
    QSharedMemory sharedMemory;
    sharedMemory.setKey("BoilerInstance");
    if (sharedMemory.attach())
        return 0;
    if (!sharedMemory.create(1))
        return 0;

    if (argc == 2)
    {
        // started from daemon
        if (!strcmp(argv[1], "-refresh"))
            s_refreshOnly = true;
    }

    // init qt
    QApplication qa(argc, argv);

    // init steam
    if (SteamAPI_RestartAppIfNecessary(k_uAppIdInvalid))
        return 1;
        

    if (!SteamAPI_Init())
    {
        Error("Fatal Error", "Steam must be running (SteamAPI_Init() failed).\n");
        return 1;
    }
        

    if (!SteamUser()->BLoggedOn())
    {
        Error("Fatal Error", "Steam user must be logged in (SteamUser()->BLoggedOn() returned false).\n");
        return 1;
    }
        

    bool running = true;
    auto cbthread = std::thread([&running]()
    {
        while (running)
        {
            try
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
                Steam_RunCallbacks(GetHSteamPipe(), false);
            }
            catch (BoilerException& e)
            {
                Error("Fatal Error", e.what());
                exit(1);
            }
        };
    });

    int res = 0;

    try
    {
        if (s_refreshOnly)
        {
            // make sure we are connected to the gc
            CSGOClient::GetInstance()->WaitForGcConnect();

            // refresh match list
            CSGOMatchList refresher;
            refresher.RefreshWait();
            res = 0;
        }
        else
        {
            // make sure we are connected to the gc
            CSGOClient::GetInstance()->WaitForGcConnect();

            // open the window
            UI wnd;
            wnd.show();
            res = qa.exec();
        }
    }
    catch (BoilerException& e)
    {
        Error("Fatal error", e.what());
        res = 1;
    }
    
    
    // shutdown
    running = false;
    cbthread.join();
    CSGOClient::Destroy();

    SteamAPI_Shutdown();
    return res;
}