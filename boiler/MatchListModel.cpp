/*
Boiler
Copyright (C) 2015  Ansas Bogdan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MatchListModel.h"
#include <QFile>
#include <QDateTime>
#include <QColor>

MatchListModel::MatchListModel(QObject* parent)
    :QAbstractTableModel(parent)
{
    connect(&m_matchList, SIGNAL(matchesFetched(int)), this, SLOT(onAddMatches(int)));
}

MatchListModel::~MatchListModel()
{
}


int MatchListModel::rowCount(const QModelIndex& parent /*= QModelIndex()*/) const
{
    return m_matchList.Matches().size();
}

int MatchListModel::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
    return (int)Columns::Count;
}

QVariant MatchListModel::data(const QModelIndex& index, int role /*= Qt::DisplayRole*/) const
{
    auto getOwnIndex = [](const CDataGCCStrike15_v2_MatchInfo& info)
    {
        uint32 accid = SteamUser()->GetSteamID().GetAccountID();
        for (int i = 0; i < info.roundstats().reservation().account_ids().size(); ++i)
            if (info.roundstats().reservation().account_ids(i) == accid)
                return i;
        throw "unable to find own accid in matchinfo";
    };

    auto getMatchResult = [&](const CDataGCCStrike15_v2_MatchInfo& info)
    {
        if (info.roundstats().match_result() == 0)
            return "tie";
        if (info.roundstats().match_result() == 1 && getOwnIndex(info) <= 4)
            return "win";
        if (info.roundstats().match_result() == 2 && getOwnIndex(info) >= 5)
            return "win";
        return "loss";
    };

    auto& match = m_matchList.Matches()[m_matchList.Matches().size() - index.row() - 1];

    if (role == Qt::DisplayRole)
    {
        switch ((Columns)index.column())
        {
        case Columns::Date:
        {
            QDateTime timestamp;
            timestamp.setTime_t(match.matchtime());
            return timestamp.toString(Qt::SystemLocaleShortDate);
        }

        case Columns::Result:
            return QString("%1 (%2:%3)")
                .arg(getMatchResult(match))
                .arg(match.roundstats().team_scores(getOwnIndex(match) >= 5 ? 1 : 0))
                .arg(match.roundstats().team_scores(getOwnIndex(match) >= 5 ? 0 : 1));

        case Columns::Link:
            return match.roundstats().map().c_str();

        case Columns::Kills:
            return match.roundstats().kills(getOwnIndex(match));

        case Columns::Assits:
            return match.roundstats().assists(getOwnIndex(match));

        case Columns::Deaths:
            return match.roundstats().deaths(getOwnIndex(match));

        case Columns::MVPs:
            return match.roundstats().mvps(getOwnIndex(match));

        case Columns::Score:
            return match.roundstats().scores(getOwnIndex(match));

        case Columns::KD:
            return QString::number((double)match.roundstats().kills(getOwnIndex(match)) /
                                   (double)match.roundstats().deaths(getOwnIndex(match)), 'f', 2);

        }
    }

    if (role == Qt::BackgroundColorRole)
    {
        if (!strcmp(getMatchResult(match), "win"))
            return QColor(0, 255, 0, 30);
        else if (!strcmp(getMatchResult(match), "loss"))
            return QColor(255, 0, 0, 30);
    }

    return QVariant();
}

QVariant MatchListModel::headerData(int section, Qt::Orientation orientation, int role /*= Qt::DisplayRole*/) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch ((Columns)section)
            {
            case Columns::Date:
                return "Date";

            case Columns::Result:
                return "Result";

            case Columns::Link:
                return "Link";

            case Columns::Kills:
                return "Kills";

            case Columns::Assits:
                return "Assists";

            case Columns::Deaths:
                return "Deaths";

            case Columns::MVPs:
                return "MVPs";

            case Columns::Score:
                return "Score";

            case Columns::KD:
                return "K/D";
            }
        }
    }
    return QVariant();
}

void MatchListModel::RefreshData()
{
    m_matchList.Refresh();
}

void MatchListModel::onAddMatches(int num)
{
    if (!num)
        return;
    beginInsertRows(QModelIndex(), 0, num - 1);
    endInsertRows();
}
