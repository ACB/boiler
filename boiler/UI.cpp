/*
Boiler
Copyright (C) 2015  Ansas Bogdan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "UI.h"
#include <algorithm>
#include <QMessageBox>
#include <QSettings>
#include <QClipboard>
#include <QDateTime>
#include <sstream>
#include "AboutDialog.h"

UI::UI(QWidget *parent /*= nullptr*/, Qt::WindowFlags flags /*= 0*/)
    :QMainWindow(parent, flags)
{
    setupUi(this);

    connect(refreshButton, SIGNAL(clicked()), this, SLOT(refresh()));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(matchList, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onMatchListContextMenu(QPoint)));

    m_matchListModel = std::make_unique<MatchListModel>(matchList);
    matchList->setModel(m_matchListModel.get());
    matchList->setColumnWidth((int)MatchListModel::Columns::Kills, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::Assits, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::Deaths, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::MVPs, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::Score, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::KD, 60);
    matchList->setColumnWidth((int)MatchListModel::Columns::Link, 420);

#ifdef Q_OS_WIN
    connect(actionAutostart, SIGNAL(toggled(bool)), this, SLOT(setAutostart(bool)));
    actionAutostart->setChecked(IsAutostart());
#endif
}

UI::~UI()
{
}

void UI::refresh()
{
    m_matchListModel->RefreshData();
}

void UI::about()
{ 
    AboutDialog dialog;
    dialog.exec();
}


bool UI::IsAutostart()
{
#ifdef Q_OS_WIN
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    return settings.value("Boiler").isValid();
#else
    return false;
#endif
}

void UI::setAutostart(bool start)
{
    QString cmd = QApplication::applicationDirPath();
#ifdef _DEBUG
    cmd.append("/BoilerDaemond.exe");
#else
    cmd.append("/BoilerDaemon.exe");
#endif

#ifdef Q_OS_WIN
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    if (start)
        settings.setValue("Boiler", cmd);
    else
        settings.remove("Boiler");
#endif
}

void UI::onMatchListContextMenu(QPoint pos)
{
    QModelIndex index = matchList->indexAt(pos);

    QMenu* menu = new QMenu(this);
    QAction* copyLink = new QAction("Copy demo link", this);
    menu->addAction(copyLink);
    connect(copyLink, SIGNAL(triggered()), this, SLOT(onCopyDemoLink()));
    QAction* copyCSV = new QAction("Copy as CSV", this);
    menu->addAction(copyCSV);
    connect(copyCSV, SIGNAL(triggered()), this, SLOT(onCopyAsCSV()));
    menu->popup(matchList->viewport()->mapToGlobal(pos));
}

void UI::onCopyDemoLink()
{
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(m_matchListModel->MatchList().Matches().at(
        m_matchListModel->MatchList().Matches().size() -
        matchList->currentIndex().row() - 1).roundstats().map().c_str()
        );
}

void UI::keyPressEvent(QKeyEvent* e)
{
    if (e->type() == QKeyEvent::KeyPress)
    {
        if (e->matches(QKeySequence::Copy))
        {
            onCopyAsCSV();
        }
    }
    QMainWindow::keyPressEvent(e);
}

void UI::onCopyAsCSV()
{
    auto model = matchList->model();

    std::stringstream csv;
    std::string seperator = "";
    for (int i = 0; i < model->columnCount(); ++i)
    {
        csv << seperator << model->headerData(i, Qt::Horizontal).toString().toStdString();
        seperator = ",";
    }
    csv << "\n";
    auto selection = matchList->selectionModel()->selectedRows();
    for (auto& r : selection)
    {
        seperator = "";
        for (int i = 0; i < model->columnCount(); ++i)
        {
            csv << seperator << model->data(model->index(r.row(), i)).toString().toStdString();
            seperator = ",";
        }
        csv << "\n";
    }
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(csv.str().c_str());
}
