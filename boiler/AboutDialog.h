#ifndef AboutDialog_h__
#define AboutDialog_h__
#include <QDialog>
#include "ui_aboutdialog.h"

class AboutDialog : public QDialog, public Ui::AboutDialog
{
public:
    AboutDialog(QWidget* parent = nullptr, Qt::WindowFlags flags = 0)
        :QDialog(parent, flags)
    {
        setupUi(this);
    }
};

#endif // AboutDialog_h__
