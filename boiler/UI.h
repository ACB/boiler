/*
Boiler
Copyright (C) 2015  Ansas Bogdan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UI_h__
#define UI_h__
#include <QtWidgets/QMainWindow>
#include <QKeyEvent>
#include "ui_mainwindow.h"
#include "MatchListModel.h"

/**
 * the main ui
 */
class UI : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT
public:
    UI(QWidget *parent = nullptr, Qt::WindowFlags flags = 0);
    ~UI();

protected:
    virtual void keyPressEvent(QKeyEvent* e) final;

public slots:
    void refresh();
    void about();
    void setAutostart(bool start);
    void onMatchListContextMenu(QPoint pos);
    void onCopyDemoLink();
    void onCopyAsCSV();
    
private:
    bool IsAutostart();

private:
    std::unique_ptr<MatchListModel> m_matchListModel;
};

#endif // UI_h__
