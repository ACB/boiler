#** About** #
Boiler is a small tool that fetches your CS:GO match history.

[DONATE](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LC8WN4H45EM6E)

© [Ansas Bogdan](http://www.steamcommunity.com/profiles/76561197985194911/)

![boiler.png](https://bitbucket.org/repo/ojLGd9/images/3958867361-boiler.png)

#** Use at OWN risk ** #

#** Installation **#
Download [Boiler.rar](https://bitbucket.org/ACB/boiler/downloads/Boiler.rar) and [dependencies.rar](https://bitbucket.org/ACB/boiler/downloads/dependencies.rar) and extract both archives into the same directory.

#** Build instructions** #
## Dependencies ##
Boiler has the following dependencies:

* [QT5](http://qt-project.org/qt5)
* [Steamworks SDK](https://partner.steamgames.com/home)
* [protobuf](https://developers.google.com/protocol-buffers/docs/downloads)
* [cmake](http://www.cmake.org/download/)

## Building ##
1. generate project files with cmake
2. compile

#** Usage ** #
Simply start Boiler.exe and hit the refresh button to update the match list if required.

If you check Boiler->Autostart Daemon BoilerDaemon.exe will launch together with Windows and update your match list everytime you close csgo.

**Do note that Boiler will tell steam you are currently playing CS:GO so i would suggest you turn it off when you are finished.**

#** TODO **#
1. automatically archive demos
2. check matches for vac bans
3. display rank information
4. display map names