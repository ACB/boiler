#include <windows.h>
#include <thread>
#include <stdexcept>

bool CSGOIsRunning()
{
    HWND test = FindWindowW(0, L"Counter-Strike: Global Offensive");
    return test != NULL;
}

void RefreshMatchList()
{
    STARTUPINFOA si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    if (!
#ifdef _DEBUG
        CreateProcessA(NULL, "Boilerd.exe -refresh", NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)
#else
        CreateProcessA(NULL, "Boiler.exe -refresh", NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)
#endif
    )
    {
        printf("CreateProcess failed (%d).\n", GetLastError());
        return;
    }
    printf("Updating match data");
    WaitForSingleObject(pi.hProcess, INFINITE);

    DWORD exitCode;
    GetExitCodeProcess(pi.hProcess, &exitCode);
    if (exitCode != EXIT_SUCCESS)
        printf(" failed (%i)\n", exitCode);
    else
        printf(" succeeded\n");

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
}

int main(int argc, char** argv)
{
    SetConsoleTitle("Boiler Daemon");
    printf("Boiler\n"
           "Copyright(C) 2015 Ansas Bogdan\n\n"

           "This program is free software : you can redistribute it and / or modify\n"
           "it under the terms of the GNU General Public License as published by\n"
           "the Free Software Foundation, either version 3 of the License, or\n"
           "(at your option) any later version.\n\n"

           "This program is distributed in the hope that it will be useful,\n"
           "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
           "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the\n"
           "GNU General Public License for more details.\n\n"

           "You should have received a copy of the GNU General Public License\n"
           "along with this program.If not, see <http://www.gnu.org/licenses/>.\n\n\n");

    bool wasRunning = false;
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(10));
        bool isRunning = CSGOIsRunning();
        if (wasRunning && !isRunning)
            RefreshMatchList();
        wasRunning = isRunning;
    }
}